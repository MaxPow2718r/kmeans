## kmeans
A quick implementation of the
[kmeans](https://en.wikipedia.org/wiki/K-means_clustering) algorithm oriented to
animate examples.

For a better python implementation use the [sklearn
module](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html)
