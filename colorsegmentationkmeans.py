#!/usr/bin/python3

import numpy as np
import sys

from PIL import Image
from sklearn.cluster import KMeans

try:
    img = sys.argv[1]
    n_clusters = int(sys.argv[2])

except IndexError:
    raise IndexError(
            """
            At least 2 arguments are expected: the input image and the number of clusters.
                    Try: imgclustering.py input.jpg n
                    or: imgclustering.py input.jpg n output.jpg
            """
            )

try:
    out = sys.argv[3]

except IndexError:
    out = 'output_{}_clusters-{}'.format(n_clusters, img)

image = Image.open(img)
N, M = image.size

X = (np.asarray(image).reshape((N*M, 3)))

kmeans = KMeans(n_clusters=n_clusters)

kmeans.fit(X)

labeld_X = kmeans.fit_predict(X)
transformed_X = kmeans.cluster_centers_[labeld_X]

quatized_array = (
    transformed_X
    .astype('uint8')
    .reshape((M, N, 3))
)

quatized_image = Image.fromarray(np.asarray(quatized_array))
quatized_image.save(out)
