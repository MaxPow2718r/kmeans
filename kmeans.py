import matplotlib.pyplot as plt
import numpy as np
import random

from PIL import Image
from sklearn.datasets import make_blobs, make_classification

import time

def avg(points):
    """
    Calculate the average of all points given.

    Parameters
    ----------
    points : array-like of any dimension

    Returns
    -------
    center : the center based on the average of the points
    """
    dimensions = len(points[0])
    N = len(points)

    center = []

    for i in range(dimensions):
        s = 0

        for p in points:
            s += p[i]

        center.append(s / N)

    return center

def init_k(data, k):
    """
    Generate k points, based on the array-like `data'.

    Parameters
    ----------
    data : array-like of N dimensions. Usually is the `data set'
    k : an int. Is the number of clusters desire

    Returns
    -------
    centers : an array with the centers of the k points
    """
    dimensions = len(data[0])
    points = len(data)
    centers = []

    min_val = []
    max_val = []

    for i in range(dimensions):
        val = []
        for j in range(points):
            val.append(data[j][i])

        min_val.append(min(val))
        max_val.append(max(val))

    for i in range(k):
        centers.append([ random.randrange(int(min_val[j]), int(max_val[j])) \
            for j in range(dimensions) ])

    return centers

def distance(a, b):
    """
    return the euclidean distance between two points

    Parameters
    ----------
    a : array-like representing the coordinates of a point (initial point)
    b : array-like representing the coordinates of a point (final point)

    Returns
    -------
    distance : the euclidean distance of the points
    """
    dimensions = len(a)

    s = 0
    for i in range(dimensions):
        diff = (a[i] - b[i]) ** 2
        s += diff

    return np.sqrt(s)

def assign(data, centers):
    """
    Assign every point in the `data set` to the closest center

    Parameters
    ----------
    data : array-like of N dimensions
    centers : array-like. The current centers

    Returns
    -------
    assign_list : an array the same dimension of the data with the index of
    wich center every point corresponds to
    """
    assign_list = []
    index = 0

    for point in data:
        closest = distance(point, centers[index])

        for i in range(len(centers)):
            d = distance(point, centers[i])
            if d < closest:
                index = i

        assign_list.append(index)

    return assign_list

def update_centers(assignments, data):
    """
    update the centers for the method.

    Parameters
    ----------
    data : array-like. The `data-set`
    assignments : array-like. List of index of wich center every point
        corresponds to

    Returns
    -------
    new_centers : an array with the new centers.
    """
    new_centers = []
    k = 0

    while k < n_clusters:
        cluster = []
        for i, j in zip(assignments, data):
            if i == k:
                cluster.append(j)

        new_centers.append(avg(cluster))
        k += 1

    return new_centers

max_iter = 10000
n_clusters = 2
n_samples = 50
random_state = 170
delay = 1

X = np.concatenate((
    np.random.normal((1, 1), scale=0.5, size=(n_samples, 2)),
    # np.random.normal((-3, 0), scale=0.6, size=(n_samples, 2))
    np.random.normal((-1, -1), scale=0.5, size=(n_samples, 2))
    ))

# centers = [[0, 0], [0, 1]]
centers = init_k(X, n_clusters)
assign_list = assign(X, centers)

old_centers = centers

# pa otro dia
# def k_means():

plt.scatter(X[:,0], X[:,1])
plt.pause(delay)
# plt.savefig("kmeans00.png")
plt.clf()

centers_x = [ centers[x][0] for x in range(len(centers)) ]
centers_y = [ centers[y][1] for y in range(len(centers)) ]

plt.scatter(X[:,0], X[:,1])
plt.scatter(centers_x, centers_y, marker="+", s=500, c="black")
plt.pause(delay)
# plt.savefig("kmeans01.png")
plt.clf()

plt.scatter(X[:,0], X[:,1], c=assign_list)
plt.scatter(centers_x, centers_y, marker="+", s=500, c="black")
plt.pause(delay)
# plt.savefig("kmeans02.png")
plt.clf()

for i in range(max_iter):
    centers = update_centers(assign_list, X)
    assign_list = assign(X, centers)

    if old_centers == centers:
        break

    centers_x = [ centers[x][0] for x in range(len(centers)) ]
    centers_y = [ centers[y][1] for y in range(len(centers)) ]

    plt.scatter(X[:,0], X[:,1], c=assign_list)
    plt.scatter(centers_x, centers_y, marker="+", s=500, c="black")
    # plt.savefig("kmeans{:02d}.png".format(i + 3))
    plt.pause(delay)
    plt.clf()

    old_centers = centers

plt.show()
