import matplotlib.pyplot as plt
import numpy as np

from sklearn.datasets import load_iris
from sklearn.cluster import KMeans

from fcmeans import FCM

n_clusters = 3

iris = load_iris()

X = iris.data[:, :2]
y = iris.target

x_min, x_max = X[:, 0].min() - 0.5, X[:, 0].max() + 0.5
y_min, y_max = X[:, 1].min() - 0.5, X[:, 1].max() + 0.5

# kmeans
kmeans = KMeans(n_clusters=n_clusters).fit_predict(X)

# FCM
fcm = FCM(n_clusters=n_clusters)
fcm.fit(X)

fcm_centers = fcm.centers
fcm_labels = fcm.predict(X)

_, axes = plt.subplots(1, 3, figsize=(11, 5), sharex=True, sharey=True)

# Plot the training points
axes[0].scatter(X[:, 0], X[:, 1], c=y)
axes[1].scatter(X[:, 0], X[:, 1], c=kmeans)
axes[2].scatter(X[:, 0], X[:, 1], c=fcm_labels)

axes[0].set_title('Original')
axes[1].set_title('KMeans')
axes[2].set_title('FCM')

axes[0].set_xlabel("Sepal length")
axes[0].set_ylabel("Sepal width")
axes[1].set_xlabel("Sepal length")
axes[1].set_ylabel("Sepal width")
axes[2].set_xlabel("Sepal length")
axes[2].set_ylabel("Sepal width")

plt.show()
