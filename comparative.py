#!/usr/bin/python3

import time

import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import KMeans
from sklearn.datasets import (
    make_blobs,
    make_moons,
    make_circles,
    make_friedman1,
    make_s_curve,
)
from fcmeans import FCM

n_samples = 1000
n_clusters = 3
random_state = None

names = ["blobs", "moons", "circles", "friedman1", "s_curve"]

datasets = [
    make_blobs(n_samples=n_samples, random_state=random_state),
    make_moons(n_samples=n_samples, random_state=random_state),
    make_circles(n_samples=n_samples, random_state=random_state),
    make_friedman1(n_samples=n_samples, random_state=random_state),
    make_s_curve(n_samples=n_samples, random_state=random_state),
]

for i, data in enumerate(datasets):
    _, axes = plt.subplots(1, 2, figsize=(11, 5), sharex=True, sharey=True)

    X, _ = data

    start_time = time.time()
    # k-means
    kmeans = KMeans(n_clusters=n_clusters).fit_predict(X)

    kmeans_time = time.time() - start_time
    # kmeans.fit_predict(X)

    start_time = time.time()
    # fcm
    fcm = FCM(n_clusters=n_clusters)
    fcm.fit(X)

    fcm_centers = fcm.centers
    fcm_labels = fcm.predict(X)

    fcm_time = time.time() - start_time

    axes[0].scatter(X[:,0], X[:,1], c=fcm_labels)
    axes[0].set_title('FCM, time: {:.5f}'.format(fcm_time) + 'sec')

    axes[1].scatter(X[:,0], X[:,1], c=kmeans)
    axes[1].set_title('KMeans, time: {:.5f}'.format(kmeans_time) + 'sec')

    plt.savefig(names[i] + ".png")
